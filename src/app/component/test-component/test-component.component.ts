import {Component, OnInit} from '@angular/core';
import {User} from "../../models/dto/user";
import {TestServiceService} from "../../service/test-service.service";
import {delay} from "rxjs/operators";

@Component({
  selector: 'app-test-component',
  templateUrl: './test-component.component.html',
  styleUrls: ['./test-component.component.css']
})
export class TestComponentComponent implements OnInit {

  title : string;
  user : User;

  constructor(private testService : TestServiceService) { }

  ngOnInit() {
    this.title = 'qwerty'
    // this.testService.getUserById().subscribe(data => this.user = data);
    this.getUser();
  }

  getUser(){
    this.testService.getUserById().pipe(delay(500)).subscribe(data => this.title = data.name);
  }

}
