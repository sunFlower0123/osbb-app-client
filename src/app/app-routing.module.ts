import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HttpClientModule} from "@angular/common/http";
import {PageNotFoundComponent} from "./component/page-not-found/page-not-found.component";
import {AppComponent} from "./app.component";
import {TestComponentComponent} from "./component/test-component/test-component.component";
import {TicketComponent} from "./component/ticket/ticket.component";

const routes: Routes = [
  { path: '', component: AppComponent},
  { path: 'test', component: TestComponentComponent},
  { path: 'ticket', component: TicketComponent},
  { path: '**', component: PageNotFoundComponent},
  // { path: '404', component: PageNotFoundComponent},
  // { path: '**', redirectTo: '/404'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    HttpClientModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
