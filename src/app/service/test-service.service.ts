import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../models/dto/user";

@Injectable({
  providedIn: 'root'
})
export class TestServiceService {

  constructor(private httpClient: HttpClient) { }

  public getUserById(): Observable<User>{
    return this.httpClient.get<User>('http://localhost:8080/user/1');
  }


}
